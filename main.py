
import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor
import numpy as np

adc_file = open("adc.txt")
adc = adc_file.readlines()

## Constants ##
fig, (axis, fft) = plt.subplots(2, 1)
plt.subplots_adjust(hspace=0.5)
VREF = 5 #Volts
RESOLUTION = 10 #Bits

### range of points which we want to plot. #TODO: maybe it can be usefull to adjust it with sliders?
MIN = 100
MAX = 200

### List of markers to calculate difference and change colors
MARKERS = []


def onclick(event):
    print('button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
          (event.button, event.x, event.y, event.xdata, event.ydata))

    ### Get point from button event ###
    axis.plot(event.xdata, event.ydata, '.C' + str((int(len(MARKERS)/2)) % 7))
    MARKERS.append([event.xdata, event.ydata])

    ### Recalculate and show it as a legend ###
    if len(MARKERS) % 2 == 0 and len(MARKERS) != 0:
        x_len = abs(MARKERS[len(MARKERS) - 1][0] - MARKERS[len(MARKERS) - 2][0])
        y_len = abs(MARKERS[len(MARKERS) - 1][1] - MARKERS[len(MARKERS) - 2][1])
        axis.legend([("dX =" + str(x_len), "dY =" + str(y_len))])
        print(x_len, y_len)

if __name__ == '__main__':

    ### Remove newline character from list ###
    for i in range(len(adc)):
        adc[i] = float(int(adc[i][:3], 16)) * (VREF/(pow(2, RESOLUTION)))


    fig.canvas.mpl_connect('button_press_event', onclick)

    ### Plotting data from file ###
    axis.plot(np.arange(len(adc[MIN:MAX])) / 900, adc[MIN:MAX])
    axis.set_xlabel("Time [s]")
    axis.set_ylabel("Voltage [V]")
    axis.set_title("Signal")

    ### Plotting FFT ###
    fft.plot(np.arange(len(adc)), abs(np.fft.fft(adc))/abs(max(np.fft.fft(adc))))
    fft.set_title("FFT")

    cursor = Cursor(axis)


    plt.show()
